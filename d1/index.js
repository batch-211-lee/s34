/*
	use "require" directive to load the express module/package
	- a "module" is a software component or part of a program that contains one or more routines
	- this is used to get the contents of the package to be used by our application
	- it also allows us to access methods and functions that will allow us to easily create a server
*/

const express = require("express");

/*
	create an application using express
	- this creates an express application and stores this in a constant called app
	- layman's term, app is our server
*/

const app = express();

// For our application server to run, we need a port to listen to

const port = 3000;

//MiddleWares
/*
	set up for allowing the server to handle data from requests
	- allows your app to read json data
	- methods used from express.js are middlewares
	-middleware is a layer of software that enables interaction and transmission of information between assorted application
*/

app.use(express.json());

/*
	Allows your app to read data from forms
	- by default, information received from the URL can only be received as a string or array
	-by aplying the option of "extend:true" we are allowed to receive information in other data types such as an object throughout our application
*/

app.use(express.urlencoded({extended : true}));

//Routes
/*
	Express has methods corresponding to each HTTP method
	- This route expects to receive a GET request at the base URL
	- The full base URL for our local application for this route will be at "http://localhost:3000"
*/

//Returns simple message
/*
	This route expects to receive a GET request at the base URL "/"

	Postman
	url: http://localhost:3000/
	method: GET
*/
app.get("/", (request, response) =>{
	response.send("Hello World")
});

//Return simple Message
/*
	uri: /hello
	method: GET

	postman:
	url: http://localhost:3000/hello
	method: GET
*/
app.get("/hello", (request, response) => {
	response.send("Hellow from the '/hello' endpoint")
});

//return simple greeting
/*
	uri: /hello
	method: POST

	postman:
	url: http://localhost:3000/hello
	method: POST
	body: raw + json
	{
		"firstName" : "Waldrich",
		"lastName" : "Lee"
	}
*/
app.post("/hello", (request,response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! This is from "/hello" endpoint but with a post method`)
});

let users = []

//Register User route
/*
	This route expects to receive a post request at URI "/register"
	- This will create a user object in the "users" variable that mirrors a real world registration process

	URI: /register
	method: "POST"

	POSTMAN:
	URL: http://localhost:3000/register
	method: POST
	body: raw + JSON
	{
		"username" : "Waldrich",
		"password" : " "
	}
*/

app.post("/register", (request, response) => {
	if(request.body.username !== " " && request.body.password !== " "){
		users.push(request.body) 
		response.send(`User ${request.body.username} successfully registered!`)
		console.log(request.body)
	} else {
		response.send("Please input Both username and password")
	}
});

//Change password
/*
	This route expects to receive a PUT request at the uri "/change-password"
	-this will update the password of a user that matches the information provided in the client/postman

	URI : /change-password
	method: PUT

	postman:
	url: http://localhost:3000/change-password
	body: raw + json
	{
		"username" : "Waldrich",
		"password" : "Wall33"
	}
*/
app.use("/change-password", (request,response) => {
	//Creates a variable to store the message to be sent back to the client/Postman
	let message;
	console.log("works afte message")

	//Creates a for loop that will loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++){
		console.log("Works before if")
		if (request.body.username == users[i].username){
			//Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = request.body.password
			//change the message sent if the password has been updated
			message = `User ${request.body.username}'s password has been updated.`
			//breaks out of the loop once the use that matches the username provided in the client/postman is found
			break;
		} else {
			//changes the message to be sent back by the response
			message = "User does not exist."
		}
	}
	console.log("works before end")
	//Sends a response back to the client/postman once the password has been updated or if a user has been found
	response.send(message)
})



/*
	tells our server to listen to the port
	- if the port is accessed, we can run the server
	- returns a massage to confirm that the server is running in the terminal
*/

app.listen(port, () => console.log(`Server running at port ${port}`))