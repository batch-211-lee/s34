const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended : true}));

let users = []
	// "username" : "johndoe",
 //    "password" : "johndoe123"


app.get("/home", (request, response) => {
	response.send("Welcome to the home page")
});

app.get("/users", (request,response) => {
	response.send(users)
});

app.post("/signup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== "") {
		users.push(request.body)
		response.send(`Thank you ${request.body.username}, you have successfully signed up.`)
	} else {
		response.send("Please input Both username and password")
	}
});

app.delete("/delete-user", (request, response) => {
	response.send("The user has been deleted")
});

app.listen(port, () => console.log(`server running at port ${port}`))